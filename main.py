import numpy as np
from read_from_file import *

def statistic_from_data(data):
    """ Statistic """
    print(data.values)

def main():
    data = read_data_from_dir('data\\32')
    statistic_from_data(data)

if __name__ == '__main__':
    main()
