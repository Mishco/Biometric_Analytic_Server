import os
import pandas as pd

def read_data_from_dir(path='32'):
    # Read all data file in directory path
    arrays_of_files = [os.path.join(path, f) for f in os.listdir(path)]

    # Read into data
    frame = pd.DataFrame()
    list_ = []
    for file_ in arrays_of_files:
        df = pd.read_csv(file_,delimiter=';')
        print(df.shape)
        list_.append(df)
    frame = pd.concat(list_)
    return frame